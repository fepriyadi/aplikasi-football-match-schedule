package com.dicoding.aplikasifootballmatchschedule.api

import android.net.Uri
import com.dicoding.aplikasifootballmatchschedule.BuildConfig
import com.dicoding.aplikasifootballmatchschedule.model.TeamResponse
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.net.URL

/**
 * Created by developer on 9/17/18.
 */
interface ApiRepository {

    @GET("eventsnextleague.php")
    fun getFiveteenNextEvents(@Query("id") id : String): Observable<TeamResponse>

    @GET("eventspastleague.php")
    fun getFiveteenLastEvents(@Query("id") id : String): Observable<TeamResponse>


    @GET("lookupevent.php")
    fun getDetailEventById(@Query("id") id : String): Observable<TeamResponse>

    companion object {
        fun create(): ApiRepository {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BuildConfig.BASE_URL)
                    .build()

            return retrofit.create(ApiRepository::class.java)
        }
    }
}