package com.dicoding.aplikasifootballmatchschedule.model

data class EventsItem(
	val intHomeShots: String? = null,
	val strSport: String? = null,
	val strHomeLineupDefense: String? = null,
	val strAwayLineupSubstitutes: String? = null,
	val idLeague: String? = null,
	val idSoccerXML: String? = null,
	val strHomeLineupForward: String? = null,
	val strTVStation: Any? = null,
	val strHomeGoalDetails: String? = null,
	val strAwayLineupGoalkeeper: String? = null,
	val strAwayLineupMidfield: String? = null,
	val idEvent: String? = null,
	val intRound: String? = null,
	val strHomeYellowCards: String? = null,
	val idHomeTeam: String? = null,
	val intHomeScore: String? = null,
	val dateEvent: String? = null,
	val strCountry: Any? = null,
	val strAwayTeam: String? = null,
	val strHomeLineupMidfield: String? = null,
	val strDate: String? = null,
	val strHomeFormation: String? = null,
	val strMap: Any? = null,
	val idAwayTeam: String? = null,
	val strAwayRedCards: String? = null,
	val strBanner: Any? = null,
	val strFanart: Any? = null,
	val strDescriptionEN: Any? = null,
	val strResult: Any? = null,
	val strCircuit: Any? = null,
	val intAwayShots: String? = null,
	val strFilename: String? = null,
	val strTime: String? = null,
	val strAwayGoalDetails: String? = null,
	val strAwayLineupForward: String? = null,
	val strLocked: String? = null,
	val strSeason: String? = null,
	val intSpectators: String? = null,
	val strHomeRedCards: String? = null,
	val strHomeLineupGoalkeeper: String? = null,
	val strHomeLineupSubstitutes: String? = null,
	val strAwayFormation: String? = null,
	val strEvent: String? = null,
	val strAwayYellowCards: String? = null,
	val strAwayLineupDefense: String? = null,
	val strHomeTeam: String? = null,
	val strThumb: Any? = null,
	val strLeague: String? = null,
	val intAwayScore: String? = null,
	val strCity: Any? = null,
	val strPoster: Any? = null
)
