package com.dicoding.aplikasifootballmatchschedule.model

data class TeamResponse(
	val events: List<EventsItem>
)
