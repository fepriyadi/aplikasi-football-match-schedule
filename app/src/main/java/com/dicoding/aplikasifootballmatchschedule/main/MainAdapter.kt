package com.dicoding.aplikasifootballmatchschedule.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dicoding.aplikasifootballmatchschedule.R
import com.dicoding.aplikasifootballmatchschedule.model.EventsItem
import kotlinx.android.synthetic.main.main_adapter.view.*
import org.jetbrains.anko.*


/**
 * Created by developer on 9/17/18.
 */
class MainAdapter(private val context : Context, private val events: List<EventsItem>, private val listener : (EventsItem) -> Unit)
    : RecyclerView.Adapter<TeamViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder {
        return TeamViewHolder(LayoutInflater.from(context).inflate(R.layout.main_adapter, parent, false))
    }

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        holder.bindItem(events[position], listener)
    }

    override fun getItemCount(): Int = events.size

}

class TeamViewHolder(view: View) : RecyclerView.ViewHolder(view){

    private val event_time: TextView = view.findViewById(R.id.event_time)
    private val first_team: TextView = view.find(R.id.first_team_name)
    private val second_team: TextView = view.find(R.id.second_team_name)
    private val first_team_score: TextView = view.find(R.id.first_team_score)
    private val second_team_score: TextView = view.find(R.id.second_team_score)

    fun bindItem(event: EventsItem, listener: (EventsItem) -> Unit) {

        event_time.text = event.strDate
        first_team.text = event.idHomeTeam
        first_team_score.text = event.intHomeScore
        second_team.text = event.idAwayTeam
        second_team_score.text = event.intAwayScore

        itemView.setOnClickListener{
            listener(event)
        }
    }
}