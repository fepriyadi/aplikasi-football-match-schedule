package com.dicoding.aplikasifootballmatchschedule.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.R.attr.colorAccent
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Spinner
import com.dicoding.aplikasifootballmatchschedule.R
import com.dicoding.aplikasifootballmatchschedule.api.ApiRepository
import com.dicoding.aplikasifootballmatchschedule.model.EventsItem
import com.dicoding.aplikasifootballmatchschedule.util.invisible
import com.dicoding.aplikasifootballmatchschedule.util.visible
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

/**
 * Created by developer on 9/17/18.
 */
class MainActivity : AppCompatActivity(), MainView{

    private lateinit var presenter: MainPresenter
    private lateinit var adapter: MainAdapter
    private lateinit var listTeam: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private var leagueId: String = "441613"
    private var eventList: MutableList<EventsItem> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = MainAdapter(this, eventList){

        }

        linearLayout {
            lparams (matchParent, wrapContent)
            orientation = LinearLayout.VERTICAL
            topPadding = dip(16)
            leftPadding = dip(16)
            rightPadding = dip(16)

            swipeRefresh = swipeRefreshLayout {
                setColorSchemeResources(R.color.colorAccent,
                        android.R.color.holo_green_light,
                        android.R.color.holo_orange_light,
                        android.R.color.holo_red_light)

                relativeLayout{
                    lparams (width = matchParent, height = wrapContent)

                    listTeam = recyclerView {
                        lparams (width = matchParent, height = wrapContent)
                        layoutManager = LinearLayoutManager(ctx)
                    }

                    progressBar = progressBar {
                    }.lparams{
                        centerHorizontally()
                    }
                }
            }
        }

        adapter = MainAdapter(this, eventList){
            toast("${it.intRound}")
        }
        listTeam.adapter = adapter

//        presenter = MainPresenter(this),
//        presenter.getFiveteenNextEvents(leagueId)

        swipeRefresh.onRefresh {
//            presenter.getFiveteenNextEvents(leagueId)
        }
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showEventList(data: List<EventsItem>) {
        swipeRefresh.isRefreshing = false
        eventList.clear()
        eventList.addAll(data)
        adapter.notifyDataSetChanged()
    }


}