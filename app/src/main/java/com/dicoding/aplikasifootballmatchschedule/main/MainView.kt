package com.dicoding.aplikasifootballmatchschedule.main

import com.dicoding.aplikasifootballmatchschedule.model.EventsItem
import java.util.*

/**
 * Created by developer on 9/17/18.
 */
interface MainView {
    fun showLoading()
    fun hideLoading()
    fun showEventList(data: List<EventsItem>)
}